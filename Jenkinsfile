pipeline {
    agent any
    tools { 
        maven 'Maven3'
    }
    options {
        // This is required if you want to clean before build
        skipDefaultCheckout(true)
    }
    stages {
        stage ('Initialize') {
            steps {
                sh '''
                    echo "PATH = ${PATH}"
                    echo "M2_HOME = ${M2_HOME}"
                ''' 
            }
        }
        stage('Build') {
            steps {
                // Clean before build
                cleanWs()
                // We need to explicitly checkout from SCM here
                checkout scm
                echo "Building ${env.JOB_NAME}...."
                sh 'mvn -B clean verify -DskipTests'
                // -B batch mode makes Maven less verbose
                sh 'cp target/SM703-1.0-SNAPSHOT.jar ./sm703.jar'
            }
        }
        stage('Test') {
            steps {
                echo "Testing ${env.JOB_NAME}...."
                sh 'mvn test'
            }
        }
        stage('Deploy') {
            steps {
                echo "Deploying ${env.JOB_NAME}..."
            }
        }
    }
    post {
        // Clean after build
        always {
            cleanWs(cleanWhenNotBuilt: false,
                    deleteDirs: true,
                    disableDeferredWipeout: true,
                    notFailBuild: true,
                    patterns: [[pattern: '.gitignore', type: 'INCLUDE'],
                               [pattern: '.propsfile', type: 'EXCLUDE']])
        }
    }
}
