package tr.edu.metu.sm703;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class SM703ExampleTest {

    private SM703Example sm703Example = new SM703Example();

    @Test
    public void addCorrect(){
        assertEquals(sm703Example.add(1, 2), 3);
    }

    @Test
    public void addInCorrect(){
        assertNotEquals(sm703Example.add(1, 2), 4);
    }

}
