package tr.edu.metu.sm703;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.util.Map;

import static java.util.Objects.isNull;

public class SM703Example implements RequestHandler<Map<String, Integer>, String> {

    public Integer add(Integer o1, Integer o2){
        return o1 + o2;
    }

    @Override
    public String handleRequest(Map<String,Integer> input, Context context) {
        return "Response is:" + add(input.get("o1"), input.get("o2"));
    }
}
